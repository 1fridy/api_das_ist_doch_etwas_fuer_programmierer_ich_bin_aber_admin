#!/bin/bash
#Labeldrucker
drucker="<drucker>"
#Verbindungsangaben Netbox
server="http://localhost:8000/api/dcim/"
token="0123456789abcdef0123456789abcdef01234567"
csrftoken="q6OatVJbNTeizDBpVNS8cpxZ4GllqMzaxRzyoCV5GzWN02rQPof4kfbKbMEupXZV"
#erster übergabewert ist cabelid
cable_id=$1
#Hole die Informationen zu der Cabelid 
#Rückgabe in Json speicher in Variable
cable_json=$(curl -s -k -X GET "${server}/cables/$cable_id/" -H "accept: application/json" -H "Authorization: Token ${token}" -H "X-CSRFToken: ${csrftoken}" )
#bestimme Device 1 id
device1_id=$( jq ".termination_a.device.id"  <<< $cable_json )
#bestimme Device 1 Name
device1_name=$( jq ".termination_a.device.name"  <<< $cable_json | sed 's/\"//g')
#bestimme Device 1 Port
device1_port=$( jq ".termination_a.name"  <<< $cable_json | sed 's/\"//g')
#bestimme Device 2 ID
device2_id=$( jq ".termination_b.device.id"  <<< $cable_json )
#bestimme Device 2 Name
device2_name=$( jq ".termination_b.device.name"  <<< $cable_json | sed 's/\"//g' )
#bestimme Device 2 Port
device2_port=$( jq ".termination_b.name"  <<< $cable_json | sed 's/\"//g' )

# Hole Informationen zu Device 1
device1_json=$(curl -s -k  -X GET "${server}/devices/$device1_id/" -H "accept: application/json" -H "Authorization: Token ${token}" -H "X-CSRFToken: ${csrftoken}" ) 
#bestimme Device 1 Standort
device1_site=$( jq '.site.name' <<< $device1_json | sed 's/\"//g')
#bestimme Device 1 Rack
device1_rack=$( jq '.rack.name' <<< $device1_json | sed 's/\"//g')
#bestimme Device 1 Position
device1_postion=$( jq '.position' <<< $device1_json )
# wenn position null, dann kann es inerhalb eines Anderen Gerätes sein z.B. Blade im Bladecenter
if [[ $device1_postion == "null" ]]
then
	# bestimme Device 1 übergeordnetets Gerät
	device1_parent_device=$(jq '.parent_device' <<< $device1_json )
	if [[ $device1_parent_device != "null" ]]
	then
	        # bestimme Device 1 übergeordnetets Gerät id
		device1_parent_device_id=$(jq '.id' <<< $device1_parent_device )
	        # bestimme Device 1 übergeordnetets Gerät Name
		device1_parent_device_name=$(jq '.name' <<< $device1_parent_device | sed 's/\"//g')
	        # bestimme Device 1 übergeordnetets Gerät url
		device1_parent_device_url=$(jq '.url' <<< $device1_parent_device | sed 's/\"http\:/\https\:/; s/\"//')
	        # hole Informationen zu Device 1 übergeordnetets Gerät
		device1_parent_device_json=$(curl -s -k  -X GET "$device1_parent_device_url" -H "accept: application/json" -H "Authorization: Token ${token}" -H "X-CSRFToken: ${csrftoken}")
	        # bestimme Device 1 übergeordnetets Gerät Position
		device1_parent_device_position=$( jq '.position' <<< $device1_parent_device_json | sed 's/\"//g')
	        # bestimme Device 1 übergeordnetets Gerät Rack
		device1_parent_device_rack=$( jq '.rack.name'  <<< $device1_parent_device_json | sed 's/\"//g')
	        # bestimme Device 1 übergeordnetets Gerät Standort
		device1_parent_device_site=$( jq '.site.name'  <<< $device1_parent_device_json | sed 's/\"//g')

	fi
	
fi
device1_postion=$(sed 's/\"//g' <<< $device1_postion)


# Hole Informationen zu Device 2
device2_json=$(curl -s -k  -X GET "${server}/devices/$device2_id/" -H "accept: application/json" -H "Authorization: Token ${token}" -H "X-CSRFToken: ${csrftoken}" ) 
#bestimme Device 2 Standort
device2_site=$( jq '.site.name' <<< $device2_json | sed 's/\"//g')
#bestimme Device 2 Rack
device2_rack=$( jq '.rack.name' <<< $device2_json | sed 's/\"//g')
#bestimme Device 2 Position
device2_postion=$( jq '.position' <<< $device2_json )
# wenn position null, dann kann es inerhalb eines Anderen Gerätes sein z.B. Blade im Bladecenter
if [[ $device2_postion == "null" ]]
then
	# bestimme Device 2 übergeordnetets Gerät 
	device2_parent_device=$(jq '.parent_device' <<< $device2_json )
	if [[ $device2_parent_device != "null" ]]
	then
		# bestimme Device 2 übergeordnetets Gerät id
		device2_parent_device_id=$(jq '.id' <<< $device2_parent_device )
		# bestimme Device 2 übergeordnetets Gerät id
		device2_parent_device_name=$(jq '.name' <<< $device2_parent_device | sed 's/\"//g')
		# bestimme Device 2 übergeordnetets Gerät url
		device2_parent_device_url=$(jq '.url' <<< $device2_parent_device | sed 's/\"http\:/\https\:/; s/\"//')
		# hole Informationen zu Device 2 übergeordnetets Gerät
		device2_parent_device_json=$(curl -s  -k  -X GET "$device2_parent_device_url" -H "accept: application/json" -H "Authorization: Token ${token}" -H "X-CSRFToken: ${csrftoken}")
		# bestimme Device 2 übergeordnetets Gerät Position
		device2_parent_device_position=$( jq '.position'  <<< $device2_parent_device_json | sed 's/\"//g')
		# bestimme Device 2 übergeordnetets Gerät Rack
		device2_parent_device_rack=$( jq '.rack.name' <<< $device2_parent_device_json | sed 's/\"//g')
		# bestimme Device 2 übergeordnetets Gerät Standort
		device2_parent_device_site=$( jq '.site.name' <<< $device2_parent_device_json | sed 's/\"//g')

	fi
	
fi


device2_postion=$(sed 's/\"//g' <<< $device2_postion)

#ersetze RZ namer durch Kürzel
case "$device1_parent_device_site" in
RZ\ Leipzig*) device1_parent_device_site="RZ LPZ1";;
POP\ Leipzig\*) device1_parent_device_site="RZ LPZ2";;
esac

case "$device1_site" in
RZ\ Leipzig*) device1_parent_device_site="RZ LPZ1";;
POP\ Leipzig\*) device1_parent_device_site="RZ LPZ2";;
esac

case "$device2_parent_device_site" in
RZ\ Leipzig\ Messeallee*) device1_site="RZ LPZ1";;
POP\ Leipzig\ Lößnig*) device1_site="RZ LPZ2";;
esac
case "$device2_site" in
RZ\ Leipzig\ Messeallee*) device1_site="RZ LPZ1";;
POP\ Leipzig\ Lößnig*) device1_site="RZ LPZ2";;
esac

### Printer Sprache zpl für Zepra labeldrucker
### Zebra biete dokumentaion: www.zebra.com/content/dam/zebra/manuals/printers/common/programming/zpl-zbi2-pm-en.pdf
### software:www.zebra.com/us/en/about-zebra/company-information/legal/open-source-usage.html
### Labeldesigner und eigenes Label nach vorlieben designen.
echo "
CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW551
^LL0256
^LS0
^FT35,233^AAB,18,10^FH\^FD$device1_name^FS
^FT53,233^AAB,18,10^FH\^FDIF: $device1_port^FS

" | tee print_"$cable_id".zpl
if [[ $device1_postion == "null" ]]
then
echo "

^FT71,233^AAB,18,10^FH\^FD+$device1_parent_device_name^FS
^FT89,233^AAB,18,10^FH\^FD$device1_parent_device_site^FS
^FT107,233^AAB,18,10^FH\^FDRack: $device1_parent_device_rack U $device1_parent_device_position^FS
" | tee -a print_"$cable_id".zpl
else
echo "

^FT71,233^AAB,18,10^FH\^FD$device1_site ^FS
^FT89,233^AAB,18,10^FH\^FDRack: $device1_rack^FS
^FT107,233^AAB,18,10^FH\^FDU $device1_postion^FS

" | tee -a print_"$cable_id".zpl
fi
echo "
^FT162,233^AAB,18,10^FH\^FD$device2_name^FS
^FT180,233^AAB,18,10^FH\^FDIF: $device2_port^FS
" | tee -a print_"$cable_id".zpl
if [[ $device2_position == "null" ]]
then
echo "

^FT198,233^AAB,18,10^FH\^FD+$device2_parent_device_name^FS
^FT216,233^AAB,18,10^FH\^FD$device2_parent_device_site^FS
^FT234,233^AAB,18,10^FH\^FDRack: $device2_parent_device_rack U $device2_parent_device_position^FS
^FO6,6^GB126,241,2^FS
^FO129,6^GB126,241,2^FS
" | tee -a print_"$cable_id".zpl
else
echo "

^FT198,233^AAB,18,10^FH\^FD$device2_site^FS
^FT216,233^AAB,18,10^FH\^FDRack $device2_rack^FS
^FT234,233^AAB,18,10^FH\^FDU $device2_postion^FS
^FO6,6^GB126,241,2^FS
^FO129,6^GB126,241,2^FS

" | tee -a print_"$cable_id".zpl
fi
echo "

^FT326,233^AAB,18,10^FH\^FD$device1_name^FS
^FT344,233^AAB,18,10^FH\^FDIF: $device1_port^FS
" | tee -a print_"$cable_id".zpl 
if [[ $device1_postion == "null" ]]
then
echo "

^FT362,233^AAB,18,10^FH\^FD+blc01-l1 ^FS
^FT380,233^AAB,18,10^FH\^FDLPZ1^FS
^FT398,233^AAB,18,10^FH\^FDRack: 3-17 U 23^FS
" | tee -a print_"$cable_id".zpl
else
echo "
^FT362,233^AAB,18,10^FH\^FD$device1_site ^FS
^FT380,233^AAB,18,10^FH\^FDRack: $device1_rack^FS
^FT398,233^AAB,18,10^FH\^FDU $device1_postion^FS
" | tee -a print_"$cable_id".zpl
fi

echo "

^FT453,233^AAB,18,10^FH\^FD$device2_nam^FS
^FT471,233^AAB,18,10^FH\^FDIF: $device2_port ^FS
" | tee -a print_"$cable_id".zpl

if [[ $device2_position == "null" ]]
then
echo "
^FT489,233^AAB,18,10^FH\^FD+$device2_parent_device_name ^FS
^FT507,233^AAB,18,10^FH\^FD$device2_parent_device_site^FS
^FT525,233^AAB,18,10^FH\^FDRack: $device2_parent_device_rack U $device2_parent_device_position^FS
^FO297,6^GB126,241,2^FS
^FO420,6^GB126,241,2^FS
^PQ1,0,1,Y^XZ
" | tee -a print_"$cable_id".zpl
else
echo "

^FT489,233^AAB,18,10^FH\^FD$device2_site^FS
^FT507,233^AAB,18,10^FH\^FDRack $device2_rack^FS
^FT525,233^AAB,18,10^FH\^FDU $device2_postion^FS
^FO297,6^GB126,241,2^FS
^FO420,6^GB126,241,2^FS
^PQ1,0,1,Y^XZ
" | tee -a print_"$cable_id".zpl
fi


#ausdrucken
lpr -P ${drucker} -o raw print_"$cable_id".zpl
# zweiter ausdruck
lpr -P ${drucker} -o raw print_"$cab e_id".zpl
