# API das ist doch etwas für ProgrammiererInnen. Ich bin aber Admin.

  

Projek zum CLT Vortrag 2022 https://chemnitzer.linux-tage.de/2022/de/programm/beitrag/136

  
  
  

## Installation

  

herunterladen der Skripte

    git clone git@gitlab.com:1fridy/api_das_ist_doch_etwas_fuer_programmierer_ich_bin_aber_admin.git

  
  

## Nutzung

  

    bash <skript>

  

oder

  

    chmod +x <skript>
    
    ./skript

  

## Support

  

issue tracker

  

## Roadmap

  

Keine

  
  

## Autor

  

Andreas Fritzsche

  

## Lizenz
- Präsentation:  [CC BY-SA 4.0 de](https://creativecommons.org/licenses/by-sa/4.0/de/legalcode)
- Skripte:  [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0)


  
## Quellen

  

### Bilder in Präsentation: 
- Screenshots der lokalen Installationen; 
- Screenshot Browser: https://raw.githubusercontent.com/komikoni/redmine-swagger/master/swagger.yaml
- Screenshot Browser: https://editor.swagger.io/
  
### lokale Installationen
- Netbox: https://netbox.readthedocs.io/en/stable/
	- Container: https://github.com/netbox-community/netbox-docker
- Redmin: https://www.redmine.org
	- Container: https://hub.docker.com/_/redmine

###  Mein Netbox/DCIM Vortrag 

https://chemnitzer.linux-tage.de/2019/de/programm/beitrag/246

### Links zu den im Vortrag aufgeführten Tools
 - https://curl.se/  curl 
 - https://www.gnu.org/software/wget/  wget 
 - https://de.wikipedia.org/wiki/Liste_von_Webbrowsern  Browser 
 - https://stedolan.github.io/jq/  jq 
 - https://github.com/jmespath/jp  jp 
 - https://albfan.github.io/RedmineJavaCLI/  RedminJavaCLI  
 - https://pypi.org/project/Redmine-CLI-Tool/  Redmine-CLI-Tool  
 - https://pypi.org/project/redman/  redman  
 - https://pypi.org/project/git-redmine/  git-redmine  
 - https://pypi.org/project/redmine-ci/  redmine-ci 
 - https://pypi.org/project/nbcli/  nbcli
